//所有的辅助类物体
import {
    AxesHelper,
    GridHelper,
    Object3D,
    PointLightHelper,
    SpotLightHelper
}from 'three'

import { pointLight, spotLight } from './Tlights'

export const helperList: Object3D[] = []

//场景辅助坐标：网格300，30个格子
const axesHelper: AxesHelper = new AxesHelper(120)
const gridHelper: GridHelper = new GridHelper(300,30,'rgb(200,200,200)','rgb(100,100,100)')

//点光辅助
const pointLightHelper: PointLightHelper = new PointLightHelper(pointLight,pointLight.distance,pointLight.color)

//聚光辅助
const spotLightHelper: SpotLightHelper = new SpotLightHelper(spotLight,spotLight.color)
//聚光灯辅助变化
// setTimeout(()=>{
//     spotLightHelper.update()
// },2000)

helperList.push(axesHelper,spotLightHelper)