// 所有场景中的光源
import {
    AmbientLight,
    Object3D,
    PointLight,
    SpotLight
}from 'three'
import { wall } from './TBasicObject'

export const lightsList: Object3D[] = []

//环境光:场景中能看到的物体的色块强度都是相同的，看起来不会有阴影
const ambientLight: AmbientLight = new AmbientLight('rgb(255,255,255)',0.3)
//0-255 -->0-1
//0,1,1
//1,0,0
//0,0,0

//0,1,1
//0.59,0.59,0.59 -->灰色
//0,0.59,0.59 -->青色

//点光
export const pointLight:PointLight = new PointLight(
    'rgb(255,0,0)',
    0.7,
    50,
    0.1
)
pointLight.position.set(20,20,20) // 点光位置


//聚光灯
export const spotLight: SpotLight = new SpotLight(
    'rgb(255,255,255)',
    1,
    500,
    Math.PI / 180 * 30,
    0,
    0
)
spotLight.position.set(0,100,470) //聚光灯位置
spotLight.castShadow = true // 造成阴影效果
spotLight.target = wall


//光源加入到lightsList中
lightsList.push(ambientLight,spotLight)