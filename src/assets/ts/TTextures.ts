import { Texture, TextureLoader } from "three";

const textTureLoader: TextureLoader = new TextureLoader()
export const pictureTexture: Texture = textTureLoader.load('/1.jpg')

export const frameColorTexture = textTureLoader.load('/WoodFloor024_1K_Color.jpg')
export const frameRoughnessTexture = textTureLoader.load('/WoodFloor024_1K_Roughness.jpg')
export const frameDispTexture = textTureLoader.load('/WoodFloor024_1K_Displacement.jpg')
