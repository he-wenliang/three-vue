import {
    Mesh,
    BoxBufferGeometry,
    MeshStandardMaterial,
    SphereBufferGeometry,
    CylinderBufferGeometry,
    Object3D,
    Line,
    Points,
    PointsMaterial,
    PlaneBufferGeometry,
    Color
} from 'three'
import { pictureTexture } from './TTextures'
import { VertexNormalsHelper } from 'three/examples/jsm/helpers/VertexNormalsHelper'

export const basicObjectList: Object3D[] = []

//地面
const stage: Mesh = new Mesh(
    new BoxBufferGeometry(600,10,300),
    new MeshStandardMaterial({
        color:'rgb(0,75,75)',
        roughness:0
    })
)
stage.castShadow = true
stage.receiveShadow = true// 接收阴影效果
// console.log((stage.material as MeshStandardMaterial).color);

stage.position.y = -5

//图片
const picture: Mesh = new Mesh(
    new PlaneBufferGeometry(192,108),
    new MeshStandardMaterial({
        //color:'red'
        map: pictureTexture
    })
)
picture.position.y = 120 // 位置
picture.position.z = -70
picture.scale.set(0.31,0.31,0.31)

//墙
export const wall:Mesh = new Mesh(
    new BoxBufferGeometry(600,200,10),
    new MeshStandardMaterial({
        color: 'white'
    })
)
wall.position.y = 100
wall.position.z = -80
//聚光灯辅助变化
wall.updateMatrix()
wall.updateMatrixWorld()

basicObjectList.push(stage,picture,wall)