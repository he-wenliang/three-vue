import { BufferAttribute, BufferGeometry, Color, Mesh, MeshStandardMaterial, Object3D } from "three";
import { pictureTexture } from "./TTextures";
import { VertexNormalsHelper } from 'three/examples/jsm/helpers/VertexNormalsHelper'

export const codeModelList: Object3D[] = []
const size: number = 10
//顶点信息
const points: Float32Array = new Float32Array([
    -size, size, size,
    size, size, size,
    size, size, -size,
    -size, size, -size,
  
    -size, -size, size,
    size, -size, size,
    size, -size, -size,
    -size, -size, -size,
  
    -size, size, size,
    -size, size, -size,
    -size, -size, -size,
    -size, -size, size,
  
    size, size, size,
    size, size, -size,
    size, -size, -size,
    size, -size, size,
  
    -size, size, size,
    size, size, size,
    size, -size, size,
    -size, -size, size,
    
  
    -size, size, -size,
    size, size, -size,
    size, -size, -size,
    -size, -size, -size,
    
  ])
  //法线
  const normals: Float32Array = new Float32Array([
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
  
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
  
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
  
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
  
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
  
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
  ])
  //
  const uv: Float32Array = new Float32Array([
    0, 0,
    1, 0,
    1, 1,
    0, 1,
  
    0, 0,
    1, 0,
    1, 1,
    0, 1,
  
    0, 0,
    1, 0,
    1, 1,
    0, 1,
  
    0, 0,
    1, 0,
    1, 1,
    0, 1,
  
    0, 0,
    1, 0,
    1, 1,
    0, 1,
  
    0, 0,
    1, 0,
    1, 1,
    0, 1,
  ])
  
  const index: number[] = [
    0, 1, 2,
    2, 3, 0,
  
    4, 5, 6,
    6, 7, 4,
  
    8, 9, 10,
    10, 11, 8,
  
    12, 14, 13,
    14, 12, 15,
  
    16, 18, 17,
    18, 16, 19,
  
    20, 21, 22,
    22, 23, 20
  ]  

  const geometry: BufferGeometry = new BufferGeometry()

  geometry.setAttribute('position', new BufferAttribute(points, 3))// 位置信息
  geometry.setAttribute('normal', new BufferAttribute(normals, 3))// 法线信息
  geometry.setAttribute('uv', new BufferAttribute(uv, 2))
  
  geometry.setIndex(index)
  
  const material: MeshStandardMaterial = new MeshStandardMaterial({
    color: 'white',
    // map: pictureTexture
  })
  
  const codeBox: Mesh = new Mesh(geometry, material)
  
  codeBox.position.y = 10
  
  // codeBox.rotation.x = Math.PI / 180 * 90
  
  const boxNormalHelper = new VertexNormalsHelper(codeBox, 10, new Color('blue').getHex())
  
  // codeModelList.push(codeBox, boxNormalHelper)